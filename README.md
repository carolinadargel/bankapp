# bankApp

This project was developed to solve a problem of accepting money in automatic cash resgisters in supermarkets. 

By implementing this program, an automatic cash register will be able to calculate the change to it's consumer, returning a given sum with the minimum number of coins and notes.

For this exercise, it's been cosidered tha the automatic teller machine contains only coins **2€**, banknotes **5€,** and **10€**, ando all these coins and notes are available in an **unlimited quantity**.

To make the application easyer to test, there is an HTML file attached, in which there is a input number to insert the value that need to be changed. The answer can be visualized at the browser console.

This project was build in JavaScript only.

About the code: 

1. The class Bills defines default values for each bill or coin that can be given as change (ten, five, two).
2. The changeMoney Bills's method receives the total value to change as it's argument. The function validates the value, returning *null* if change the value with the available bills and coins is not possible. 
3. The billDistribution Bill's method distribute the withdraw value in bills of 10, 5 and coins of two.
4. The remainUpdate method is called inside the billDistribution method, and subtract value already distributed in bills from remain value, and if remain value isn't 0, the bills and notes distribution need to be recalculated on the billDistribution method.
5. The reset method reset default values in order to be able to calculate a new withdraw.

This system works for any value higher than 0. 

Thanks for the opportunity!

**Carolina Dargel**
*carolinadargel@gmail.com*



