const Bills = {
    two: 0, 
    five: 0,
    ten: 0,
    remain: 0,

    
    changeMoney(withdraw) {

        this.reset();
        this.remain = withdraw;
        
        //if change is impossible, return null
        if (this.remain == 0 || this.remain == 1 || this.remain == 3)  return null
        
        this.billDistribution(this.ten, 10)
        this.billDistribution(this.five, 5)
        this.billDistribution(this.two, 2)

        console.log(this.result())

        return this.result()
    },
       

    //Distribute value in 10, 5 and 2
    billDistribution(billAmount, billValue) {
        if (this.remain / billValue >= 1) {
            if (billValue === 10) {
                this.ten = Math.floor(this.remain / billValue)
                billAmount = this.ten
            }
            if (billValue === 5) {
                this.five = Math.floor(this.remain / billValue)
                billAmount = this.five
            }
            if (billValue === 2) {
                this.two = Math.floor(this.remain / billValue)
                billAmount = this.two
            }
            this.remainUpdate(billAmount, billValue)
        }
        
        //treat result when remain value is 1, by subtracting a 5 or a 10 bill
        // and converting respective value in coins of 2
        if (this.remain === 1) {
            if (this.five > 0) {
                this.five = this.five - 1
                this.two = this.two + 3
            }
            else if (this.ten > 0) {
                this.ten = this.ten - 1
                this.five = this.five + 1
                this.two = this.two + 3
            }

            this.remain = 0
        }
    },


    //subtract value already distributed in bills from remain value
    remainUpdate(billAmount, billValue) {
        this.remain = this.remain - (billAmount * billValue);
    },


    //reset bills to 0 in order to iniciate a new calcule
    reset() {
        this.two = 0
        this.five = 0
        this.ten = 0
    },

    
    //format Object to desirable result
    result() {
        return {
            two: this.two,
            five: this.five,
            ten: this.ten,
        }
    }
}

